// alert("hi") 

//Arrays
	// [] -array literals


// Arrays and Indexes [0,1,2,3]
	// Reassign values in an array


myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake react"
]

console.log(myTasks);
myTasks[0] = "sleep for 8 hours";
console.log(myTasks);


// Reading from Arrays / Accessing from an array
console.log(myTasks[2]);


//getting the length of an array
console.log(myTasks.length);

if (myTasks.length > 3) {
	console.log("We have too many tasks. Please contact Simon.")
}

// Accessing last element in an array

let lastElement = myTasks.length -1
console.log(myTasks[lastElement]);


// Array Methods - help us manipulate our arrays
	// 1. Mutator Methods -functions that change an array

	let fruits = ["Apple", "Blueberry", "Oranges", "Grapes"]

		//push -adds an element in the end of an array and returns the array's length
		console.log(fruits);
		let fruitslength = fruits.push("Mango");

		fruits.push("Guava", "Kiwi");
		console.log(fruits);


		//pop - removes the last element in an array and returns the removed element

		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log(fruits);

		fruits.pop();
		console.log(fruits);


		// unshift - adds an element/s at the beginning of an array

		fruits.unshift("Guava");
		console.log(fruits); 

		fruits.unshift("Kiwi", "Lime");
		console.log(fruits);

		// shift - removes an element/s at the beginning of an array
		fruits.shift();
		console.log(fruits);

		// splice - simultaneously removes elements from a specified index number and adds elements
			/*
			Syntax:
				arrayName.splice(startingIndex,deleteCount, elementsToBeAdded)
			*/

			fruits.splice(1,2,"Cherry", "Watermelon");
			console.log(fruits);

			//remove multiple elements (from index 3 to the last element)
			fruits.splice(3)
			console.log(fruits);

			fruits.splice(1,1);
			console.log(fruits);


			//add in the middle
			fruits.splice(2,0,"Cherry", "Buko");
			console.log(fruits);

		// Sort - rearranges the array elements in alphanumeric order

		fruits.sort();
		console.log(fruits);

		// reverse method 

		fruits.reverse();
		console.log(fruits);

	// 2. Non-mutator Methods - do not change the array

		// 


	let countries = [
		"US",
		"PH",
		"CAN",
		"SG",
		"TH",
		"PH",
		"FR",
		"DE"
	]

		// indexOf - returns the index number of the first matching element
		/*
		Syntax: arrayName.indexOf("element")
		*/

		console.log(countries.indexOf("PH"));
			//result: 1

		let indexOfSG = countries.indexOf("SG");
		console.log(indexOfSG);
			// result: 3

		let invalidCountry = countries.indexOf("SK");
		console.log(invalidCountry);
			// result: -1

		let invalidCountry2 = countries.indexOf("JP");
		console.log(invalidCountry2);
			// result: -1


		// a. slice - slices elements from an array and returns a new array
			/*
			Syntax: 
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
			*/

		let sliceArrayA = countries.slice(2);
		console.log(sliceArrayA);
			// result: CAN to DE

		let sliceArrayB = countries.slice(2, 4);
		console.log(sliceArrayB);
			// result: CAN and SG 

		// slice from last element - use negative number

		let sliceArrayC = countries.slice(-3);
		console.log(sliceArrayC);
			// result: PH, FR, DE



		// b. toString 
			/*
			Syntax: arrayName.toString();
			*/

			let stringArray = countries.toString();
			console.log(stringArray);

			let sentence = ["I", "like", "javascript", ".", "It's", "fun", "!"]

			let sentenceString = sentence.toString();
			console.log(sentenceString);

		// c. concat - combines 2 arrays and returns a combined result
			/*
			Syntax: arrayA.concat(arrayB);
					arrayA.concat(elementA);
			*/

			let tasksArrayA = ["drink html", "eat javascript"];
			let tasksArrayB = ["inhale css", "breathe sass"];
			let tasksArrayC = ["get git", "be node"];

			let tasks = tasksArrayA.concat(tasksArrayB);
			console.log(tasks);

			// combining multiple arrays

			let allTasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
			console.log(allTasks);

			// combining Array with elements

			let combinedTasks = tasksArrayA.concat("smell express", "throw react");
			console.log(combinedTasks);

		// d. join method - returns an array as a sting with a special separator string 
				// ('')  -no space
				// (" ")  - with space
				// ("/") - with slash

			let members = ["Rose", "Jisoo", "Jennie"];
			let joinedMembers1 = members.join();
			console.log(joinedMembers1);

			let joinedMembers2 = members.join('');
			console.log(joinedMembers2);



			let joinedMembers3 = members.join(" ");
			console.log(joinedMembers3);



		// ITERATION METHODS - have a function inside
			// loops designed to perform repetitive tasks on arrays
			// useful for manipulating array data

		// forEach()

			// empty array will store the filtered elements in the Iteration Method 
			// to avoid confusion
		let filteredTasks = [];

		allTasks.forEach(function(task) {
			console.log(task);
			
			if(task.length > 10) {
					filteredTasks.push(task);
			}
		})

		console.log(filteredTasks);

		// map()
			// similar with forEach
			// iterates for each element and returns new array with different values
			// unlike forEach, map methof requires the use of return keyword in order to 
			// create another array with the performed operation

			let numbers = [1, 2, 3, 4, 5];
			let numberMap = numbers.map(function(number){
					return number * number;
				})

			console.log(numberMap);

		// every()
			// will check text if all elements meet a certain condition
			// returns a boolean
			// best used when checkin a  large amount of data

			let allValid = numbers.every(function(number){
				return (number < 3);
			});
			console.log(allValid);
				// false

		// some()
			//

			let someValid = numbers.some(function(number){
				return (number < 2);
			})
			console.log(someValid);
				// true


		// filter()
			// returns a new array that contains the elements that meet a certain condition
			// if no elements are found, it will return an empty array

			let filterValid = numbers.filter(function(number){
				return (number < 3);
			});
			console.log(filterValid);
				// result: 1, 2

// ??????? remove values

		let colors = ["red", "green", "black", "orange", "yellow"]

		let values = ["red", "black", "yellow"];

		colors = colors.filter(item => values.indexOf(item) === -1)
		console.log(colors);

		/*
			colors = colors.filter(function(item){
				return (values.indexOf(item) === -1)
			})
		*/


		// includes()
			// returns a booleanvalue true if it finds a matching item in the array
			// includes us case sensitive

			let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

			let filteredProducts = products.filter (function(product){
				return product.toLowerCase().includes("a");
			});

			console.log(filteredProducts);

		// Multidimensional Array
			// useful for storing complex data structure
			// array within an array

			let chessboard = [
				["a1", "b1", "c1", "d1", "e1", "f1", "g1"],
				["a2", "b2", "c2", "d2", "e2", "f2", "g2"],
				["a3", "b3", "c3", "d3", "e3", "f3", "g3"],
				["a4", "b4", "c4", "d4", "e4", "f4", "g4"],
				["a5", "b5", "c5", "d5", "e5", "f5", "g5"],
				["a6", "b6", "c6", "d6", "e6", "f6", "g6"],
				["a7", "b7", "c7", "d7", "e7", "f7", "g7"],
				["a8", "b8", "c8", "d8", "e8", "f8", "g8"],
			]

			console.log(chessboard[1][4]);
				// result: e2

			console.log(`Queen moves to ${chessboard[5][5]}`)










			

			



























